## Building

To work with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://jekyllrb.com/) Jekyll
1. Generate the website: `jekyll build -d public`
1. Preview your project: `jekyll serve`
1. Add content

Read more at Jekyll's [documentation](https://jekyllrb.com/docs/home/).
