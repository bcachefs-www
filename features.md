---
layout: page
title: Features and Progress
permalink: /features/
---
### Complete

- Full data checksumming: Fully supported and enabled by default. We do need to implement scrubbing, once we've got replication and can take advantage of it.

- Encryption: Implementation is finished, and passes all the tests. More review would be much appreciated.

### In progress

- Compression: Not quite finished - it's safe to enable, but there's some work left related to copy GC before we can enable free space accounting based on compressed size: right now, enabling compression won't actually let you store any more data in your filesystem than if the data was uncompressed

- Tiering: Works (there are users using it), but recent testing and development has not focused enough on multiple devices to call it supported. In particular, the device add/remove functionality is known to be currently buggy.

- Multiple devices, replication: Roughly 80% or 90% implemented, but it's been on the back burner for quite awhile in favor of making the core functionality production quality - replication is not currently suitable for outside testing.

- Snapshots: Snapshot implementation has been started, but snapshots are by far the most complex of the remaining features to implement - it's going to be quite awhile before I can dedicate enough time to finishing them, but I'm very much looking forward to showing off what it'll be able to do.

