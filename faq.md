---
layout: page
title: FAQ
permalink: /faq/
---

**Q. Why do we need _another_ CoW filesystem?**

A. There many needs that are currently unmet by current filesystems and we think making a new one is the best way to change that.

**Q. What can bcachefs do that other filesystems can't?**

A. bcachefs excels at tiering, high and consistent performance, flexibility of replication and is being thoroughly tested to make sure it never corrupts data.

**Q. What is the current status of bcachefs development?**

A. As of writing, bcachefs is being actively developed. Please see the Features and Progress page for more details.
