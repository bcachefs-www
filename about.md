---
layout: page
title: About
permalink: /about/
---

Bcachefs is a new b-tree based CoW filesystem by Kent Overstreet, author of the widely used bcache kernel module. Its goals are, in order of priority:

- To be reliable. Your data shouldn't be lost due to a filesystem bug. Ever. This isn't like that _other_ CoW filesystem.

- To be fast, consistently. Not only is latency lower than other filesystems but bcachefs' architecture enables latency to be more consistent as well.

- To be flexible. You want to move from a mirrored replication scheme to erasure coding? You can do that. You want to encrypt at the filesystem level and take advantage of AEAD encryption? You can do that too. You want a 3-tier filesystem with high capacity but low speed nearline drives in an erasure coded configuration at the bottom, higher speed 15k enterprise drives mirrored in the middle and highest speed NVMe SSD or RAMdisk on the top? bcache _is_ the filesystem now.

TODO(Kent): Explain how bcachefs started.
